<!DOCTYPE html>
<html>
<head>
    <title>Market Wala</title>

    <link href="{{url()}}/assets/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
    <script src="{{url()}}/assets/js/jquery.min.js"></script>
    <link href="{{url()}}/assets/css/style.css" rel="stylesheet" type="text/css" media="all" />  
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <script type="application/x-javascript"> 
    addEventListener("load", function() { 
        setTimeout(hideURLbar, 0);

    }, false);

    function hideURLbar(){ 
        window.scrollTo(0,1); 
    }

    $(document).ready(function(){
        $(".memenu").memenu();
    });
    </script>
    <!-- Css will goes here-->
    @yield('libraryCSS')
    <link href="{{url()}}/assets/css/memenu.css" rel="stylesheet" type="text/css" media="all" />
    <script type="text/javascript" src="{{url()}}/assets/js/memenu.js"></script>
    <script src="{{url()}}/assets/js/simpleCart.min.js"> </script>
    <script src="{{url()}}/assets/js/responsiveslides.min.js"></script>
       <script>
        $(function () {
          $("#slider").responsiveSlides({
            auto: true,
            speed: 500,
            namespace: "callbacks",
            pager: true,
          });
        });
      </script>
</head>
    <body>
    <!-- including nav bar -->
    @include('layout.navbar')
    @yield('content')


    @yield('libraryJS')
    @include('layout.footer')
    </body>
</html>